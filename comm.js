#!/usr/bin/env node

const smd = require('./index')
const fs = require('fs')
const path = require('path')
const gw = require('glob-watcher')
const lodash = require('lodash')
const inq = require('inquirer')
const sm = new smd()
let userConfs
let userConfsFile = path.join(process.cwd(), 'summaryConfig.js')
function outConfsJS(redo) {
    let opt = arguments[0] || false
    let str = `
    // you can add more property if you need(Different from defaults). but don't del the default propertys(you can change the value).
// Why not JSON? For the summarymd module, you can redefine anything and use JavaScript files to customize configuration files with greater freedom. 
// If you need more customization, consider using the summarymd module instead of the command line.

const confs ={
    // 
    // it's just a default string ,you can do more. the "%title%" will be replaced by true title. 
    template: "# %title%",
    // 要包含的文件描述(gulp到路径描述规则)。WARN：必须是数组，会被解构。
    includes: ['./**/*.md', './**/*.markdown'],
    // 要排除的文件描述（gulp到路径描述规则）。WARN：必须是数组，会被解构。
    excludes:['!./node_modules/**/*.*'],
    // 正式目录文件。NOTE: 运行时会被添加到 excludes[]
    // Official directory file path.
    summary: "./SUMMARY.md",
    confs_with_create: {
        /**
         * 在创建文件时是否跳过URL为空或者链接文本为空的列表。
         * Whether to skip lists with empty URLs or empty link text when creating files.
         */
        isSkipEmptyTitleOrEmptyPath:true,
        // 远程路径匹配规则。可能不完全匹配出来，请自行调整。
        // Remote Path Matching Rules--This package does not handle any remote paths unless the matched rules fail to match.
        remoteURLregexp: /(file\\:\\/\\/)|\\:.*|\\..*[\\/|\\?\\#].*\\..|((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})(\\.((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})){3}/,
        },
    confs_with_summary: {
        // 临时目录文件。NOTO: 运行时会被添加到 excludes
        // Temporary directory file path
        tempSummary: "./_summary.md",
        // 是否使用基础文件名作为链接文本。
        // Whether to use the base file name as link text.
        isUseBFasLinkText: false,
        // Markdown 列表标记符。
        // Markdown List Markup Symbol
        listSing: "*",
        // 是否对链接进行 URL 编码。
        // 
        isEncodeURI: false,
        indent: {
            // 是否进行缩进, 'false'的话，后续的缩进设置会被忽略，不会执行缩进。 
            // Whether to perform URL encoding for links in the directory list.
            isIndent: true,

            // 单次缩进的空格个数。 本模块的实际缩进量 = indentLength * times，照目前的配置，缩进2次就是缩进 8 个空格。
            // Number of spaces indented once. The actual indentation of this module = indentLength * times. According to the current configuration, indentation twice is to indent eight spaces.
            indentLength: 4, 
            
            /* 
            下面两个配置项控制自定义缩进规则，基本格式为：
             
              [{basis:/npm/g,times:1}]
            
             basis -缩进规则，类型：正则表达式
             times -缩进次数（注意：不是缩进量是缩进次数）
            
            上面例子表示： 匹配到 "npm" 就缩进一次。
            注意：你不能同时设置它们（IndentByDirs 和 IndentByTitle），只能选其中一种方式来进行生成临时目录文件。如果不需要自定义缩进规则的话将两者都设置为null。

            The following two configurations control custom indentation rules in the basic format:
            
                [{basis:/npm/g, times:1}]

            basis - indentation rule, type: regular expression
            times - Indentation times.
            
            The above example shows that the matching to "npm" is indented once.

            NOTE:You can't set them（IndentByDirs and IndentByTitle） up at the same time. You can only choose one way to generate temporary directory files. Set both to null if you don't need to customize indentation rules.
            */
            IndentByDirs: null,
            IndentByTitle: null,
        }
    }
}
module.exports = {
    confs
}
    `
    let confsExit = fs.existsSync(path.join(process.cwd(), 'summaryConfig.js'))
    if (opt || confsExit == false) {
        fs.writeFileSync("summaryConfig.js", str, { flag: 'w' })
    }

}
try {
    userConfs = require(userConfsFile)
} catch (e) {
    console.log(`   ${__filename}:17 --> ${e}`)
    outConfsJS()
    console.log('   file: summaryConfig.js created. You need rerun it!')
}
sm.configs = userConfs.confs
let handleDocs = [...sm.configs.excludes, '!' + sm.configs.summary, '!' + sm.configs.confs_with_summary.tempSummary,...sm.configs.includes]

function creater() {
    sm.create((t) => { let template = ""; template = sm.configs.template; return template.replace(/(\%title\%)/g, t) })
}
function watcher() {
    let iexcludes = [...sm.configs.excludes, '!' + sm.configs.summary, '!' + sm.configs.confs_with_summary.tempSummary]
    let souc = lodash.flattenDeep([sm.configs.includes, iexcludes])
    let watcher = gw(souc)
    watcher.on('add', () => {
        sm.update()
        sm._summaryStatus_hasProblems()
    })
    watcher.on('unlink', () => {
        sm.update()
        sm._summaryStatus_hasProblems()
    })
    let summWatcher = gw(sm.configs.summary)
    summWatcher.on('change', () => {
        sm.create((t) => { let template = ""; template = sm.configs.template; return template.replace(/(\%title\%)/g, t) })
        sm._summaryStatus_hasProblems()
    })
}
const commands = [
    { option: "init", eva: "outConfsJS()", des: "创建配置文件：summaryConfig.js" },
    { option: "reinit", eva: "outConfsJS(true)", des: '恢复配置文件 summaryConfig.js ' },
    { option: "create", eva: "creater()", des: "创建文件" },
    { option: "update", eva: "sm.update()", des: "更新目录" },
    { option: "status", eva: "sm._summaryStatus_hasProblems()", des: "检查目录" },
    { option: "watch", eva: "watcher()", des: "启用监视" },
    { option: "docSources", eva: "console.log(handleDocs)", des: "当前配置下，所覆盖到的 Markdown 文件路径规则描述，包括包含的和排除的，用于检查是否有遗漏" },
]
/**
 * 
 * @param {string} s1 
 * @param {string} s2 
 * @param {number} len 
 * @returns 
 */
function desF(s1, s2, len) {
    let rp = len - s1.length
    return `${s1} ${" ".repeat(rp)} ${s2}`
}
let mes = lodash.map(commands, o => {
    return `${desF(o.option, o.des, 15)}`
})
mes = mes.join('\n  ')+"\n_____(Press ⬆️ ⬇️ to choose)____"

inq.prompt({
    type: 'list',
    name: 'chose',
    choices: lodash.map(commands, o => o.option),
    message: mes
}).then(answ => {
    let pick = answ.chose
    let commObj = lodash.find(commands, { "option": pick })
    let action = commObj.eva
    try {
        eval(action)
    } catch {
        console.log(`   ${__filename}:163 --> ${JSON.stringify(commObj)}`)
    }
}).catch(e => {
    console.log(`   ${__filename}:166 --> ${e}`)
})